import ServiceMasterReqSchemaModel from "../mapping/validators/Service_Master_GBO.validator";
import servicemaster from "./Service_Master_GBO.json";

describe("ServiceMaster GBO Validator Test",  () => {
    
    test("[CREATE] Basic validation test", async()=> {        
        try{
            let output = new ServiceMasterReqSchemaModel().validateCreate(servicemaster);            
            expect(output).toBeDefined();            
            console.log(output["service-master"].srv_id);
        }catch(error){
            fail(error);
        }        
    });

    test("[CREATE] Basic validation failure", async()=> {
        let sample:string = '{"service-master":{}}';
        try{
            let output = new ServiceMasterReqSchemaModel().validateCreate(JSON.parse(sample));            
            expect(output).toBeDefined();            
            expect(output["metadata"].status).toBeDefined();
        }catch(error){
            console.log(`Error: ${error}`);
            fail(error);
        }        
    });
});