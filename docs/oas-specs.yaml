openapi: 3.0.3
# Added by API Auto Mocking Plugin
servers:
  - description: Local Development Server
    url: https://localhost/api/service-master/0.1.0-alpha01
  - description: Sanbox Environment
    url: https://api.openbanking.com/service-master/0.1.0-alpha01
info:
  description: | 
      Service master `API` allows management of of a service definition in the `kadal0g` service registry. This API is the core of the service registry as it allows managing of the master record. All other modules will feed from the meta-content collected in the service kadal0g to perform required operation.      
      # Structural Definition
      ## Attributes
      This entity will represent all attributes required to define a service. Attributes includes:

      | Field Name         | Data Type   | Mandatory / Optional | Description                                                                                                                                                                                                                                                                                            |
      |--------------------|-------------|----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
      | service_id         | String(56)  | M                    | UUI / Unique Identifier for defining a service.                                                                                                                                                                                     |
      | name               | String(128) | M                    | Name of the service example: `finances`.                                                                                                                                                                                                                                                               |
      | title              | String(256) | M                    | Title of the service. It can be a single line title, describing the service component. Example: `Service Component for Managing Services related to Finances`                                                                                                                                          |
      | group_slug         | String(128) | M                    | Service Group Slug. Group name will be converted by the system by converting spaces to `-`.                                                                                                                                                                                                            |
      | group_name         | String(256) | M                    | Service Group name. Example `Account Management`                                                                                                                                                                                                                                                       |
      | imp_tech_uri       | String(256) | M                    |  URI for defining [implementation technology](https://gitlab.com/openfintechlab/medulla/project-docs/-/wikis/Kadl0g:-Service-Catalog/Appendix:-URI-Definitions#imp_tech_uri)                                                                                                                           |
      | source_uri         | String(256) | O                    | URI for defining [source tech](https://gitlab.com/openfintechlab/medulla/project-docs/-/wikis/Kadl0g:-Service-Catalog/Appendix:-URI-Definitions#source_uri)                                                                                                                                            |
      | ctx_root_uri       | String(256) | O                    | URI for defining [context root](https://gitlab.com/openfintechlab/medulla/project-docs/-/wikis/Kadl0g:-Service-Catalog/Appendix:-URI-Definitions#ctx_root_uri)                                                                                                                                         |
      | sdk_doc_uri        | String(256) | O                    | URI for defining [sdk documentation](https://gitlab.com/openfintechlab/medulla/project-docs/-/wikis/Kadl0g:-Service-Catalog/Appendix:-URI-Definitions#sdk_doc_uri). SDK Documentation can contain all docs related to Classes, procedures, modules in a specific service component. Example: `JS-Docs` |
      | api_doc_uri        | String(256) | O                    | URI for defining [API documentation](https://gitlab.com/openfintechlab/medulla/project-docs/-/wikis/Kadl0g:-Service-Catalog/Appendix:-URI-Definitions#api_doc_uri). SWAGGER docs are expected here.                                                                                                    |
      | visibility         | String(16)  | M                    | `private` or `public`. All public service components will be exposed to the `service gateway`, hence they will be accessible from outside URL by the channel.                                                                                                                                          |
      | type               | String(64)  | M                    | Type of service. It can include: - infra - functional -service- business-service - technical-service - aggregator                                                                                                                                                                                      |
      | lifecycle          | String(16)  | M                    | Lifecycle of the service: - production - sit - dev - uat                                                                                                                                                                                                                                               |
      | discussion_chl_uri | String(256) | O                    | URI for defining [Dicussion Channel](https://gitlab.com/openfintechlab/medulla/project-docs/-/wikis/Kadl0g:-Service-Catalog/Appendix:-URI-Definitions#discussion_chl_uri).                                                                                                                             |
      | tags               | String(128) | O                    | Service Tags represented by hash. Example #poc                                                                                                                                                                                                                                                         |
      | doc_md_b64             | CLOB        | O                    | This section will contain md representation of the service documentation in base 64 encoded format                                                                                                                                                                                                                               |
      | doc_md_file_name             | String(64)        | O                    | This section will contain markdown file name                                                                                                                                                                                                                               |


      ## Procedures
      | Functiona Name | Description                                                                  |
      |----------------|------------------------------------------------------------------------------|
      | create()       | Creates Service Component                                                    |
      | update()       | Updates Service Component fields. Any Field can be updated                   |
      | delete()       | Delete Service Component using service id                                    |
      | get()          | Get service component using service_id or service_name                       |
      | get()?with_doc | Get service component detail including docs using service_id or service_name |


      ## Usage of URIs in the Message Structure
      URI are used in service definitions to expand the field without adding new attributes. Format used for defining uri is `<key1>:<value1>;<key2>:<value2>`.

      Example usage is:
      ```javascript
      {
        "id": "123",
        "name" : "testing",
        "src_type_uri" : "type:node.js;ver:>12.0.0;isContainer:true"
      }
      ```
      In the above example, you can easily see that field `src_type_uri` is expanded using key-value pairs and new keys can be added without changing the message structure.

      ### imp_tech_uri

      This attribute is used to define the Implementation Technology Details.

      | Key             | Value Description                                                                                      | Example               |
      |-----------------|--------------------------------------------------------------------------------------------------------|-----------------------|
      | type            | Implementation technology type. This can contain common langauge names like `java`, `c`, `go-lang` etc | type: nodejs          |
      | version         | framework version. Example `openjdk-13`, `>12.0.0` (for nodejs) etc                                    | version: >12.0.0      |
      | isContainerized | Is the service containerized (true of false)                                                           | isContainerized: true |
      

      ### source_uri
      This attribute is used to define `source code uri`.

      | Key         | Value Description                            | Example                                                          |
      |-------------|----------------------------------------------|------------------------------------------------------------------|
      | repo_type   | Repository Type. `github` , `gitlab`, `jira` | repo_type: gitlab                                                |
      | repo_tech   | Reposity Tech. `git`, `svn`                  | repo_tech: git                                                   |
      | repo_url    | URL of the repository                        | repo_url: git@gitlab.com:openfintechlab/medulla/project-docs.git |
      | repo_branch | Name of the branch                           | repo_branch: master                                              |


      ### ctx_root_uri
      This element does not contain values in `key/value` pair. It is kept as `uri` for future usage only.
      
      ### sdk_doc_uri
      This element describes `sdk documentation` URI and is used to define the location where the sdk documentation is hosted for this specific component.

      | Key  | Value Description                                                                                                | Example         |
      |------|------------------------------------------------------------------------------------------------------------------|-----------------|
      | url  | URL of the docs either defined in relative path OR in the absolute path. Type of path is defined by attribute `type` | url: /docs/api  |
      | type | `relative` or `absolute`                                                                                        | type: relative |

      ### api_doc_uri
      This element describes `api documentation` URI and is used to define the location where the api documentation is hosted for this specific component.

      | Key  | Value Description                                                                                                | Example         |
      |------|------------------------------------------------------------------------------------------------------------------|-----------------|
      | url  | URL of the docs either defined in relative path OR in the absolute path. Type of path is defined by attribute `type` | url: /docs/api  |
      | type | `relative` or `absolute`                                                                                        | type: relative |

      ### discussion_chl_uri
      This element describes the discussion channel associated with the specific service.

      | Key  | Value Description                                                            | Example     |
      |------|---------------------------------------------------------------------------------------------------|-------------|
      | url  | Channel Name  / URL or other location uniquely defining the specific channel | url: test   |
      | type | `slack`, `gitter`, `stack-exchange` etc                                      | type: slack |

      # Creation of Service using `medctl` utility
      YAML specification is used to create a service using `medctl` utility. 
      ```yaml
      apiVersion: "v0.1.0PreAlpha"
      apiEndpoint:
          host: "192.168.159.132"
          contextRoot: "/service-master-ora"
          protocols: "http"
          port: 3000
          method: POST
      expectedResponse:
          codes:
          - 200
          - 201        
      payload:
        service-master:
              srv_id: cf5a359c-d467-4371-bf41-99ad73e000002
              name: finances30124311
              title: Service Component for managing finances
              group_slug: finance-management
              group_name: Finance Management
              imp_tech_uri: 'type: nodejs;version:>10.0.0;isContainerized:true'
              source_uri: repo_type:gitlab;repo_tech:git;repo_url:git@gitlab.com:openfintechlab/medulla/applications/finances.git;repo_branch:master
              ctx_root_uri: "/finances2"
              sdk_doc_uri: "/docs/"
              api_doc_uri: "/api/docs"
              visibility: private
              type: business
              lifecycle: dev
              discussion_chl_uri: type:slack;url:finances
              tags: "#medulla,#service-component"
              doc_md_b64: IyBUYWJsZSBPZiBDb250ZW50CltbX1RPQ1
              doc_md_file_name: test.md

      ```    
      ## Utility Syntax      
      ```
      medctl [option] <YAML Spec File>
      ```
      where  `option` and `YAML Spec File` are:
      - `option`: Specifies the option you want to perform on the resource. Currently, this supports the flag `-f` only.
      - `YAML Spec File`: `YAMl` specification having details of the endpoint and `HTTP-Operation`

  version: "0.1.0-Alpha01"
  title: API for creating Service Definition
  contact:
    name: API Support
    email: contact@openfintechlab.com
  license:
    name: MIT-License
    url: 'https://opensource.org/licenses/MIT'
tags:
  - name: service-master
    description: Create, Retrieve, Update and Delete operation related to `service-master`
paths:
  /service-master:
    post:
      parameters:
        - in: query
          name: generateid
          description: 'If set, system automatically generates the unique UUID as a service ID and provides the generated id in the response object. **NOTE:** No value is required in this paramter'
          schema:
            type: string
          required: false
      tags:
        - service-master
      summary: 'Create New Service'
      operationId: addServiceMaster
      description: 'Creates service-master in the underlying data-store'
      responses:
        '201':
          description: 'service-master created'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'
        '500':
          description: 'Error while created service-master'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'
        
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ServiceMaster'
        description: Inventory item to add  
    put:
      tags:
        - service-master
      summary: 'Updates service record'
      operationId: updateServiceMaster
      description: 'Updates service-master entries in the data-store'
      responses:
        '201':
          description: 'service-master created'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'
        '500':
          description: 'Error while created service-master'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'
        
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ServiceMaster'
        description: Inventory item to add  
    get:
      summary: |
        Gets all service records from the data store.
        This service is pagination enabled and can be controlled using `from` and `to` paramters. 
        **For Example:** 
        - For top 10 use https://example.com/service-master?from=1&to=20 
        - For top10 and having `visibility` public and `lifecycle` as `public` and `dev` respectively, use: https://example.com/service-master?from=1&to=20&visibility=private&lifecycle=dev 
        > NOTE: Maximum of 100 records can be retrieved in one call.
      parameters:
        - in: query
          name: from
          description: 'From record count. This parameter controls the paging of the records at service side. If not set, it will be defaulted to 1. Example: If top 10 records are required to be fetched, this parameter should be set as 1 and parameter to should be set as 10.'
          schema:
            type: integer
          required: false
        - in: query
          name: to
          description: 'to record count. This parameter controls the paging of the records at service side. If not set, it will be defaulted to 1. Example: If top 10 records are required to be fetched, this parameter should be set as 1 and parameter to should be set as 10.'
          schema:
            type: integer
          required: false          
        - in: query
          name: visibility
          description: 'Get all service-master records where vsbility criteria matches the provided string. Example: `private` or `public`'
          schema:
            type: string
          required: false                  
        - in: query
          name: lifecycle
          description: 'Get all service-master records where lifecycle criteria matches the provided string. Example: `dev` or `production`'
          schema:
            type: string
          required: false                    
      tags:
        - service-master
      responses:
        '200':
          description: 'All instances of services retrieved. MAX 100 will be returned'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ServiceMasterArray'
        '500':
          description: 'Error while retriving services records from the underline data store'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'      
        '404':
          description: 'Records not found. This condition will occur in-case no record exist in the datbase based on the procided criteria.'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'      
  /service-master/{id}:
    delete:
      tags:
        - service-master
      summary: 'Delete service entry from the database. The delete will only happens when child records in the subsequent data tables are deleted as well.'
      parameters:
        - name: id
          in: path
          description: 'UUID representing the service ID'
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '201':
          description: 'service-master created'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'
        '404':
          description: 'Records not found. This condition will occur in-case no record exist in the datbase based on the procided criteria.'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'  
        '500':
          description: 'Error while created service-master'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'
    get:
      tags:
        - service-master
      parameters:
        - in: path
          name: id
          description: 'UUID representing the service ID. This method gets only one service record using the service ID'
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          description: 'Services retrieved'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ServiceMasterArray'
        '404':
          description: 'Records not found. This condition will occur in-case no record exist in the datbase based on the procided criteria.'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'    
        '500':
          description: 'Error while created service-master'
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/response-object'
      summary: 'Gets all service-master records'
components:
  schemas:
    ServiceMaster:
      type: object      
      properties:
        service-master: 
          type: object
          required:
           - srv_id
           - name
           - title
           - group_slug
           - group_name
           - imp_tech_uri
           - visibility
           - type
           - lifecycle
          properties:          
            srv_id:
              type: string
              format: uuid
              description: > 
                'Unique Service ID used to identify the service. Incase of **Update**, this field should have the ID of the service record which needs to be updated  `Data Limit: 56 chars`'
              example: 'cf5a359c-d467-4371-bf41-99ad73e000002'
            name:
              type: string
              format: string
              description: 'Unique Name of the service. `Data Limit: 128 chars`'
              example: 'notification'
            title:
              type: string
              format: string
              description: 'Title of the service. It can be a single line title, describing the service component. Example: Service Component for Managing Services related to Finances. `data Limit: 256 chars`'
              example: 'Service Component for Managing Services related to Finances.'
            group_slug:
              type: string
              format: string
              description: 'Service Group Slug. Group name will be converted by the system by converting spaces to -. `Data Limit: 128 chars`'
              example: '#service-master'
            group_name:
              type: string
              format: string
              description: 'Service Group name. Example Account Management. `Data LImit: 256 chars`'
              example: 'Account Management'
            imp_tech_uri:
              type: string
              format: string
              description: |
                'Implementation Technology uri. Please refer to [link](https://gitlab.com/openfintechlab/medulla/project-docs/-/wikis/Kadl0g:-Service-Catalog/Appendix:-URI-Definitions#imp_tech_uri) for more details. `Data Limit: 256 chars`'                      
            source_uri:
              type: string
              format: string
              description: 'URI definition for source. `Data Limit: 256 chars`'
              example: 'repo_type: gitlab;repo_tech: git;repo_url: git@gitlab.com:openfintechlab/medulla/project-docs.git;repo_branch: master'
            ctx_root_uri:
              type: string
              format: string
              description: 'URI definition for context root. `Data Limit: 256 chars`'
              example: '/service-master'
            sdk_doc_uri:
              type: string
              format: string
              description: 'URI for defining sdk documentation. SDK Documentation can contain all docs related to Classes, procedures, modules in a specific service component. Example: JS-Docs. `Data limit: 256 chars`'
              example: 'url: /docs/api;type: relative'
            api_doc_uri:
              type: string
              format: string
              description: 'URI for defining API documentation. SWAGGER docs are expected here. `Data limit: 256 chars`'
              example: 'url: /help/_docs;type: relative'
            visibility:
              type: string
              enum:
                - 'private'
                - 'public'
              description: 'private or public. All public service components will be exposed to the service gateway, hence they will be accessible from outside URL by the channel. '
              example: 'private'
            type:
              type: string
              enum:
                - 'infra'
                - 'functional'
                - 'business'
                - 'technical'
              description: 'Type of service. '
              example: 'technical'
            lifecycle: 
              type: string
              enum:
                - production
                - sit
                - dev
                - uat
              example: 'SIT'
            discussion_chl_uri:
              type: string
              format: string
              description: 'URI for defining Dicussion Channel. `Data Limit: 256 chars`'
              example: 'url: test; type: slack'
            tags:
              type: string
              format: string
              description: 'Service Tags represented by hash. Example #poc. `Data Limit: 128 chars`'
              example: '#poc'
            doc_md_b64:
              type: string
              format: byte
              description: 'This section will contain md representation of the service documentation in base 64 encoded format'
            doc_md_file_name:
              type: string
              format: string
              description: 'This section will contain markdown file name. `Data limit: 64 chars`'
              example: 'test.md'
    ServiceMasterArray:
      type: object      
      properties:
        service-master: 
          type: array
          items:
            type: object
            minItems: 1
            maxItems: 100
            properties:                          
              srv_id:
                type: string
                format: uuid
                description: > 
                  'Unique Service ID used to identify the service. Incase of **Update**, this field should have the ID of the service record which needs to be updated  `Data Limit: 56 chars`'
                example: 'cf5a359c-d467-4371-bf41-99ad73e000002'
              name:
                type: string
                format: string
                description: 'Unique Name of the service. `Data Limit: 128 chars`'
                example: 'notification'
              title:
                type: string
                format: string
                description: 'Title of the service. It can be a single line title, describing the service component. Example: Service Component for Managing Services related to Finances. `data Limit: 256 chars`'
                example: 'Service Component for Managing Services related to Finances.'
              group_slug:
                type: string
                format: string
                description: 'Service Group Slug. Group name will be converted by the system by converting spaces to -. `Data Limit: 128 chars`'
                example: '#service-master'
              group_name:
                type: string
                format: string
                description: 'Service Group name. Example Account Management. `Data LImit: 256 chars`'
                example: 'Account Management'
              imp_tech_uri:
                type: string
                format: string
                description: |
                  'Implementation Technology uri. Please refer to [link](https://gitlab.com/openfintechlab/medulla/project-docs/-/wikis/Kadl0g:-Service-Catalog/Appendix:-URI-Definitions#imp_tech_uri) for more details. `Data Limit: 256 chars`'                      
              source_uri:
                type: string
                format: string
                description: 'URI definition for source. `Data Limit: 256 chars`'
                example: 'repo_type: gitlab;repo_tech: git;repo_url: git@gitlab.com:openfintechlab/medulla/project-docs.git;repo_branch: master'
              ctx_root_uri:
                type: string
                format: string
                description: 'URI definition for context root. `Data Limit: 256 chars`'
                example: '/service-master'
              sdk_doc_uri:
                type: string
                format: string
                description: 'URI for defining sdk documentation. SDK Documentation can contain all docs related to Classes, procedures, modules in a specific service component. Example: JS-Docs. `Data limit: 256 chars`'
                example: 'url: /docs/api;type: relative'
              api_doc_uri:
                type: string
                format: string
                description: 'URI for defining API documentation. SWAGGER docs are expected here. `Data limit: 256 chars`'
                example: 'url: /help/_docs;type: relative'
              visibility:
                type: string
                enum:
                  - 'private'
                  - 'public'
                description: 'private or public. All public service components will be exposed to the service gateway, hence they will be accessible from outside URL by the channel. '
                example: 'private'
              type:
                type: string
                enum:
                  - 'infra'
                  - 'functional'
                  - 'business'
                  - 'technical'
                description: 'Type of service. '
                example: 'technical'
              lifecycle: 
                type: string
                enum:
                  - production
                  - sit
                  - dev
                  - uat
                example: 'SIT'
              discussion_chl_uri:
                type: string
                format: string
                description: 'URI for defining Dicussion Channel. `Data Limit: 256 chars`'
                example: 'url: test; type: slack'
              tags:
                type: string
                format: string
                description: 'Service Tags represented by hash. Example #poc. `Data Limit: 128 chars`'
                example: '#poc'
              doc_md_b64:
                type: string
                format: byte
                description: 'This section will contain md representation of the service documentation in base 64 encoded format'
              doc_md_file_name:
                type: string
                format: string
                description: 'This section will contain markdown file name. `Data limit: 64 chars`'
                example: 'test.md'              
    response-object:
      type: object
      description: 'Metadata aggregate describing the status of the transaction. Status codes describes the error codes / business validation failure and can be mapped to a specific exception cases. Although not necessary, status code `0000` defines succesfull posting'
      properties:
        metadata:
          type: object
          properties:
            status:
              type: string
              format: string
              description: 'Four character status code. Deafults to `0000` incase of success. Other error codes includs:  `8150: Invalid request message  8151:	Error While performing the Operation 8152: Service already exist 8153: Required key does not exist in the db 8154: Schema validation error 8155: Invalid input`'
              example: '0000'
            description:
              type: string
              format: string
              description: 'Status summary describing the code. `Data Limit: 128chars`'
              example: 'Success!'
            trace:
              type: array
              items:
                type: object
                properties:
                  source:
                    type: string
                    format: string
                    example: 'db_error'
                  description:
                    type: string
                    format: string
                    example: 'Internal error from the database driver'