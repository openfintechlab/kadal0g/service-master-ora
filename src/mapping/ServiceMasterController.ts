/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Controller for managing all operations of the Service-Master
 */

import IServiceMaster                   from "./bussObj/IServiceMaster";
import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";
import OracleDBInteractionUtil          from "../utils/OracleDBInteractionUtil";
import util                             from "util";
import { v4 as uuidv4 }                 from "uuid";


 export default class ServiceMasterController{
    private readonly _MAX_RECORD_ABUSE:number = 100;    
    /**
     * Creates provided service-master entity in oracle database
     * @param reqJSON (string) JSON string of the request message
     * @async
     */
    public async create(reqJSON:string, generateSrvID:boolean = false){
        logger.debug("Request Received for creating the service-master: "+reqJSON.toString());        
        var reqServiceMaster:IServiceMaster;        
        reqServiceMaster= JSON.parse(reqJSON);                
        logger.info(`Creating Service Master with srv_id: ${reqServiceMaster["service-master"].srv_id} and name: ${reqServiceMaster["service-master"].name}`);        
        if(reqServiceMaster["service-master"] === undefined){
            throw new Error("Invalid request message. Parsing failed");
        }        
        var response!:string;
        try{
            response = await this.createServMasterReq(reqServiceMaster,generateSrvID);
            logger.info(`Service created`);
        }catch(error){
            logger.error(`Error while creating service: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            throw error;
        }        
        return response;
    }

    /**
     * Delete a service from the service component list
     * @param srv_id (string) Delete a specific Service using service id
     * @async
     */
    public async delete (srv_id:string){
        logger.info(`Deleting with srv_id: ${srv_id}`);        
        let sql:string = `DELETE FROM MED_SERVICE WHERE srv_id=:v1`;
        let binds = [srv_id];
        try{
            let impacted = await OracleDBInteractionUtil.executeUpdate(sql,binds);
            logger.debug(`Result of DB deletion: ${util.inspect(impacted,{compact:true,colors:true, depth: null})} `);
            if(impacted.rowsAffected > 0){
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("0000","Success!");   
            }                
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8153","Required key does not exist in the db");   
            }
        }catch(error){
            logger.error(`Error received while performing DELETE DB operation: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);                        
            throw error;
        }
    }

    /**
     * Update the service-master object
     * @param reqJSON (string) JSOn request object in string
     * @async
     */
    public async update(reqJSON:string){
        var reqServiceMaster:IServiceMaster;                
        reqServiceMaster= JSON.parse(reqJSON);                
        logger.info(`Creating Service Master with srv_id: ${reqServiceMaster["service-master"].srv_id} and name: ${reqServiceMaster["service-master"].name}`);                
        var response!:string;
        if(reqServiceMaster["service-master"] === undefined){
            throw new Error("Invalid request message. Parsing failed");
        }else{
            // Call the DB function            
            try{
                response = await this.updateServMasterReq(reqServiceMaster);
                logger.info(`Service Updated`);
            }catch(error){
                logger.error(`Error while updating service: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
                throw error;
            }        
        }                
        return response;
    }

    /**
     * Get the sepcific servicer from the service-master table
     * @param id (string) Service Master ID
     * @async
     */
    public async get(id:string){
        logger.info(`Getting specific servicer-master transaction`);
        let sql:string = `SELECT srv_id, "NAME", title, group_slug, group_name, imp_tech_uri, source_uri, hostname,port,ctx_root_uri, sdk_doc_uri, api_doc_uri, visibility, "TYPE", lifecycle, discussion_chl_uri, tags, doc_md_file_name, created_on, updated_on, created_by, updated_by, last_ops_id FROM med_service WHERE srv_id = :v1`;
        let binds = [id];
        try{
            let result = await OracleDBInteractionUtil.executeRead(sql,binds);
            logger.debug(`Result: ${result}`);            
            let bussObj = await this.createBusinessObject(result);
            logger.debug(`BussObj:  ${util.inspect(bussObj,{compact:true,colors:true, depth: null})}`);            
            return bussObj;
        }catch(error){
            logger.error(`Error occured while converting data to business object ${error}`)
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8151","Error While performing the Operation");
        }
    }

    /**
     * Get all met-content of the service.
     * NOTE: from and to determine the 
     * @param from (number) From record number
     * @param to (number) To record number
     * @param visibility (string) private | public
     * @param lifecycle (string) Component lifecycle (dev/prod/)
     * @async
     */
    public async getAll(from:number=1,to:number=10, visibility:string, lifecycle:string){
        logger.info(`Getting all service-master record from the db starting from: ${from} - to - ${to}`);
        to = (to > this._MAX_RECORD_ABUSE)? this._MAX_RECORD_ABUSE: to;                    
        if(from > to){
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8155","Invalid input");
        }
        let sql:string = `select RES.*,(SELECT COUNT(*) FROM med_service) as TTL_ROWS from (
            SELECT ROWNUM,srv_id, "NAME", title, group_slug, group_name, imp_tech_uri, source_uri, hostname,port, ctx_root_uri, sdk_doc_uri, api_doc_uri, visibility, "TYPE", lifecycle, discussion_chl_uri, tags, doc_md_file_name, created_on, updated_on, created_by, updated_by, last_ops_id 
            FROM med_service where visibility = coalesce(:v1,visibility) AND lifecycle=coalesce(:v2,lifecycle)
            ) RES where ROWNUM BETWEEN :v3 and :v4 order by rownum `;        
        let binds:any = [visibility, lifecycle, from,to];        
        try{
            let result = await OracleDBInteractionUtil.executeRead(sql, binds);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let busObj = await this.createBusinessObjectForAll(result);
            return busObj;
        }catch(error){
            logger.error(`Error occured while converting data to business object ${error}`)
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8151","Error While performing the Operation");
        }
    }
    
    private async createServMasterReq(reqJSON:any,generateSrvID:boolean = false){
        logger.debug(`Request received for creating record in database ${util.inspect(reqJSON,{compact:true,colors:true, depth: null})}`);        
        let srvMasterReq = reqJSON["service-master"];   
        let srvid:string = uuidv4();     
        // Step#1: Select specific record from the database
        let sqlSel:string = `SELECT srv_id FROM MED_SERVICE WHERE SRV_ID=:v1 OR "NAME"=:v2 OR CTX_ROOT_URI=:v3`;
        let result = await OracleDBInteractionUtil.executeRead(sqlSel,[(generateSrvID)?srvid:srvMasterReq.srv_id,srvMasterReq.name,srvMasterReq.ctx_root_uri]);        
        logger.debug(`DB Select for redundancy returned: ${util.inspect(result,{compact:true,colors:true, depth: null})} Record Length: ${result.rows.length}`);
        if(result.rows.length > 0 ){
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8152","Service already exist", new PostRespBusinessObjects.Trace("error-desc","Service ID, Name and context Root can not be same"));
        }
        // Step#2: Insert specific data in the database
        let sql:string = `INSERT INTO med_service (srv_id,"NAME",title,group_slug,group_name,imp_tech_uri,source_uri,ctx_root_uri,sdk_doc_uri, api_doc_uri,visibility,"TYPE",lifecycle,discussion_chl_uri,tags,doc_md_b64,doc_md_file_name,created_on,updated_on,created_by,updated_by,last_ops_id, hostname, port) VALUES(:v0,:v1,:v2,:v3,:v4,:v5,:v6,:v7,:v8,:v9,:v10,:v11,:v12,:v13,:v14,:v15,:v16,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,:v17,:v18,:v19, :v20, :v21) `;        
        let binds = [(generateSrvID)?srvid:srvMasterReq.srv_id, srvMasterReq.name, srvMasterReq.title, srvMasterReq.group_slug, 
                        srvMasterReq.group_name,srvMasterReq.imp_tech_uri,srvMasterReq.source_uri,srvMasterReq.ctx_root_uri,srvMasterReq.sdk_doc_uri,srvMasterReq.api_doc_uri,
                        srvMasterReq.visibility,srvMasterReq.type,srvMasterReq.lifecycle,srvMasterReq.discussion_chl_uri,srvMasterReq.tags,srvMasterReq.doc_md_b64,srvMasterReq.doc_md_file_name,
                        'SYSTEM','SYSTEM',null, srvMasterReq.hostname, srvMasterReq.port];
        try{
            await OracleDBInteractionUtil.executeUpdate(sql,binds);
            if(generateSrvID){
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("0000","Success!", new PostRespBusinessObjects.Trace("srv_id",srvid));
            }else{
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("0000","Success!");
            }            
        }catch(error){
            logger.error(`Error received while performing DB operation: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);                        
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8151","Error While performing the Operation");
        }                
    }

    private async updateServMasterReq(reqJSON:any){        
        logger.debug(`Request received for updating record in database ${util.inspect(reqJSON,{compact:true,colors:true, depth: null})}`);        
        let srvMasterReq = reqJSON["service-master"];        
        // Step#1: See if the name being updated already exist or not
        let sqlSel:string = `SELECT srv_id FROM MED_SERVICE WHERE srv_id=:v1`;
        let result = await OracleDBInteractionUtil.executeRead(sqlSel,[srvMasterReq.srv_id]);        
        logger.debug(`DB Select for redundancy returned: ${util.inspect(result,{compact:true,colors:true, depth: null})} Record Length: ${result.rows.length}`);
        if(result.rows.length <= 0){            
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8153","Required key does not exist in the db");
        }
        // Update the database
        let sql:string;
        let binds:any;
        if(srvMasterReq.doc_md_b64){
            sql = `UPDATE med_service SET  "NAME" = COALESCE(:v1,"NAME")  ,title = COALESCE(:v2,title)  ,group_slug = COALESCE(:v3,group_slug)  ,group_name = COALESCE(:v4,group_name)  ,imp_tech_uri = COALESCE(:v5,imp_tech_uri),source_uri = COALESCE(:v6,source_uri)  ,ctx_root_uri = COALESCE(:v7,ctx_root_uri)  , sdk_doc_uri = COALESCE(:v8,sdk_doc_uri)  ,api_doc_uri = COALESCE(:v9,api_doc_uri)  ,visibility = COALESCE(:v10,visibility) ,"TYPE" = COALESCE(:v11,"TYPE")  ,lifecycle = COALESCE(:v12,lifecycle)  ,discussion_chl_uri = COALESCE(:v13,discussion_chl_uri)  ,tags = COALESCE(:v14, tags)  ,doc_md_b64 = :v15,doc_md_file_name = COALESCE(:v16,doc_md_file_name),updated_on = CURRENT_TIMESTAMP  ,updated_by = :v18  ,last_ops_id = :v19, hostname=:v20, port=:v21 WHERE	srv_id=:v22`;
            binds = [srvMasterReq.name, srvMasterReq.title, srvMasterReq.group_slug, srvMasterReq.group_name,srvMasterReq.imp_tech_uri,srvMasterReq.source_uri,srvMasterReq.ctx_root_uri,srvMasterReq.sdk_doc_uri,srvMasterReq.api_doc_uri,srvMasterReq.visibility,srvMasterReq.type,srvMasterReq.lifecycle,srvMasterReq.discussion_chl_uri,srvMasterReq.tags,srvMasterReq.doc_md_b64,srvMasterReq.doc_md_file_name,'SYSTEM',null, srvMasterReq.hostname,srvMasterReq.port, srvMasterReq.srv_id];        
        }else{
            sql = `UPDATE med_service SET  "NAME" = COALESCE(:v1,"name")  ,title = COALESCE(:v2,title)  ,group_slug = COALESCE(:v3,group_slug)  ,group_name = COALESCE(:v4,group_name)  ,imp_tech_uri = COALESCE(:v5,imp_tech_uri),source_uri = COALESCE(:v6,source_uri)  ,ctx_root_uri = COALESCE(:v7,ctx_root_uri)  , sdk_doc_uri = COALESCE(:v8,sdk_doc_uri)  ,api_doc_uri = COALESCE(:v9,api_doc_uri)  ,visibility = COALESCE(:v10,visibility) ,"TYPE" = COALESCE(:v11,"TYPE")  ,lifecycle = COALESCE(:v12,lifecycle)  ,discussion_chl_uri = COALESCE(:v13,discussion_chl_uri)  ,tags = COALESCE(:v14, tags)  ,doc_md_file_name = COALESCE(:v16,doc_md_file_name),updated_on = CURRENT_TIMESTAMP  ,updated_by = :v18  ,last_ops_id = :v19, hostname=:v20, port=:v21 WHERE	srv_id=:v22`;
            binds = [srvMasterReq.name, srvMasterReq.title, srvMasterReq.group_slug, srvMasterReq.group_name,srvMasterReq.imp_tech_uri,srvMasterReq.source_uri,srvMasterReq.ctx_root_uri,srvMasterReq.sdk_doc_uri,srvMasterReq.api_doc_uri,srvMasterReq.visibility,srvMasterReq.type,srvMasterReq.lifecycle,srvMasterReq.discussion_chl_uri,srvMasterReq.tags,srvMasterReq.doc_md_file_name,'SYSTEM',null, srvMasterReq.hostname,srvMasterReq.port, srvMasterReq.srv_id];        
        }        
        try{
            let resp = await OracleDBInteractionUtil.executeUpdate(sql,binds);
            logger.info(`Records updated. `);
            logger.debug(`Response of update operation: ${util.inspect(resp,{compact:true,colors:true, depth: null})}`);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");            
        }catch(error){
            logger.error(`Error received while performing DB operation: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);            
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8151","Error While performing the Operation");
        }
    }

    private async createBusinessObject(dbResult:any){        
        return new Promise<any>((resolve:any,reject:any) => {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db"));
            }
            var bussObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),
                    
                },
                "service-master": {
                    "srv_id": dbResult.rows[0].SRV_ID,                    
                    "name": dbResult.rows[0].NAME,
                    "title": dbResult.rows[0].TITLE,
                    "group_slug": dbResult.rows[0].GROUP_SLUG,
                    "group_name": dbResult.rows[0].GROUP_NAME,
                    "imp_tech_uri": dbResult.rows[0].IMP_TECH_URI,
                    "source_uri": dbResult.rows[0].SOURCE_URI,
                    "hostname": dbResult.rows[0].HOSTNAME,
                    "port": dbResult.rows[0].PORT,
                    "ctx_root_uri": dbResult.rows[0].CTX_ROOT_URI,
                    "sdk_doc_uri": dbResult.rows[0].SDK_DOC_URI,
                    "api_doc_uri": dbResult.rows[0].API_DOC_URI,
                    "visibility": dbResult.rows[0].VISIBILITY,
                    "type": dbResult.rows[0].TYPE,
                    "lifecycle": dbResult.rows[0].LIFECYCLE,
                    "discussion_chl_uri": dbResult.rows[0].DISCUSSION_CHL_URI,
                    "tags": dbResult.rows[0].TAGS,
                    "doc_md_file_name": dbResult.rows[0].DOC_MD_FILE_NAME,
                    "created_on": dbResult.rows[0].CREATED_ON ,
                    "updated_on": dbResult.rows[0].UPDATED_ON ,
                    "created_by": dbResult.rows[0].CREATED_BY ,
                    "updated_by": dbResult.rows[0].UPDATED_BY ,
                    "last_op_id": dbResult.rows[0].LAST_OPS_ID

                }
            };        
            resolve(bussObj);
        });        
    }

    private async createBusinessObjectForAll(dbResult:any){
        return new Promise<any>((resolve:any,reject:any) => {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db"));
            }
            var bussObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),
                    "trace": [
                        {
                            "source": "TotalRows",
                            "description": dbResult.rows[0].TTL_ROWS
                        }
                    ]
                },
                "service-master": []
            };        
            for(let i:number =0; i< dbResult.rows.length; i++){
                bussObj["service-master"].push({ 
                    "srv_id": dbResult.rows[i].SRV_ID,                    
                    "name": dbResult.rows[i].NAME,
                    "title": dbResult.rows[i].TITLE,
                    "group_slug": dbResult.rows[i].GROUP_SLUG,
                    "group_name": dbResult.rows[i].GROUP_NAME,  
                    "hostname": dbResult.rows[i].HOSTNAME,
                    "port": dbResult.rows[i].PORT,
                    "ctx_root_uri" : dbResult.rows[i].CTX_ROOT_URI,                   
                    "api_doc_uri" : dbResult.rows[i].API_DOC_URI,                   
                    "visibility": dbResult.rows[i].VISIBILITY,
                    "type": dbResult.rows[i].TYPE,
                    "lifecycle": dbResult.rows[i].LIFECYCLE,                                        
                    "updated_on": dbResult.rows[i].UPDATED_ON ,                    
                    "updated_by": dbResult.rows[i].UPDATED_BY             
                });
            }
            resolve(bussObj);
        });
    }
    
 }