/**
  * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of the framework
 * @description
 * - Class holding static variables
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * 
 */

export default class StaticVars{
    
    /** Static keys for holding configuration variables */
    public static readonly ConfigVars = {
        "contextRoot": "oftl.gen.cfg.contextroot"
    }

    public static DefaultValues = {
        "expressAppPort": 3000
    }
}