import OracleDBInteraction from "../utils/OracleDBInteractionUtil";
import util                     from "util";
import { v4 as uuidv4 } from "uuid";

describe("Unit Test Cases for OracleDBInteraction Layer", () => {
    let dbConfig = {
        userName: "miscatalog",
        password: "m!sc@tal0g",
        connStr: "localhost:49161/XE",
        invalidConnStr: "localhost:49163/XE"
    }
    beforeAll(async ()=>{
        try{
            await OracleDBInteraction.initPoolConnections(dbConfig.userName,dbConfig.password,dbConfig.connStr)
            expect(true).toBeTruthy();
        } catch(error){
            fail(error);
        }
    });

    test("Initiate DB pool - sucess case valid endpoint", async () => {
        try{
            await OracleDBInteraction.initPoolConnections(dbConfig.userName,dbConfig.password,dbConfig.connStr)
            expect(true).toBeTruthy();
        } catch(error){
            fail(error);
        }
    });

    test("Initiate DB pool - failure case", async () => {
        try{
            await OracleDBInteraction.initPoolConnections(dbConfig.userName,dbConfig.password,dbConfig.invalidConnStr)
            fail();            
        } catch(error){
            expect(true).toBeTruthy();
        }
    });
    
    
    test("Test execution of simple READ SQL statement WITHOUT any parameter", async () => {
        const result:any = await OracleDBInteraction.executeRead("SELECT * FROM MED_SERVICE");         
        // console.log(`Complete object: ${util.inspect(result,{compact:true,colors:true, depth: null})}`);
        // console.log(`All rows : ${util.inspect(result.rows,{compact:true,colors:true, depth: null})}`);
        // console.log(`First : ${util.inspect(result.rows[0],{compact:true,colors:true, depth: null})}`);
        expect(true).toBeTruthy();
    });

    test("Test execution of simple READ SQL statement WITH any parameter", async () => {
        const sql = `SELECT * FROM MED_SERVICE WHERE ROWNUM=:a`;
        const binds:any = [1];
        const result:any = await OracleDBInteraction.executeRead(sql,binds);        
        
        // console.log(`Complete object: ${util.inspect(result,{compact:true,colors:true, depth: null})}`);
        // console.log(`All rows : ${util.inspect(result.rows,{compact:true,colors:true, depth: null})}`);
        // console.log(`First : ${util.inspect(result.rows[0],{compact:true,colors:true, depth: null})}`);
        expect(true).toBeTruthy();
    });

    test("Generating uuid()", ()=>{
        console.log(` UUID is: ${uuidv4()}`);
    })

    test("Execute Simple Insert Statement without any bind", async()=>{                
        const sql = `INSERT INTO no_example(id,data) VALUES(1,'TEST')`;        
        try{
            await OracleDBInteraction.executeRead("SELECT * FROM no_tab1");
            await OracleDBInteraction.executeUpdate( `DROP TABLE no_tab1`);        
            await OracleDBInteraction.executeUpdate( `CREATE TABLE no_tab1 (id NUMBER, name VARCHAR2(50))`);        
        }catch(error){
            // Create database            
            await OracleDBInteraction.executeUpdate( `CREATE TABLE no_tab1 (id NUMBER, name VARCHAR2(50))`);        
        }
        try{
            const result:any = await OracleDBInteraction.executeUpdate( `INSERT INTO no_tab1 VALUES (:id, :nm)`,[1,'Alison']);        
            console.log(`Result: ${util.inspect(result,{compact:true,colors:true, depth: null})}`);
            expect(true).toBeTruthy();
        }catch(error){
            console.error(`Error: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            fail(error);
        }                
        
    });
    
});