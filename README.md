# Service Master - Oracle - Service Component
[![pipeline status](https://gitlab.com/openfintechlab/kadal0g/service-master-ora/badges/master/pipeline.svg)](https://gitlab.com/openfintechlab/kadal0g/service-master-ora/-/commits/master)

Service Component representing entity `Service Master`. This component have all api's and endpoint releated to service component `Service Master`.

## Getting Started
This service component have all end point related to service-master.

## Prerequisites
- Ensure all pre-requisites are met. Refer to the shared folder `Process Catalog > Configuration Management > GitLab`
- Ensure your new service library is ready and you have initialized git repo

| Software  | Version       |
|-----------|:--------------|   
| Node      | 12.17.0 LTS   |
| Kubernetes| v1.18.2       |
| docker    | v19.0.3       |
| VS Code   | Latest        |
| yarn      | 1.22.4        |
| typescript| 3.9.3         |
> **NOTE:** Please note that the versions will keep on changing. 
## How to use the boilerplate project ?
Follow the steps below to download and use the boilerplate code:


### Testing pre-defined routes
There are three rules pre-defined in the project:
#### `/healthz` for liveness test
This is a special route and can be used for defining liveness probe. This route will always respond back with response code `200`. This route can be tuned more according to the service component requirement.
```
$ curl http://192.168.159.131:3000/service-boilerplate/healthz -v
*   Trying 192.168.159.131...
* TCP_NODELAY set
* Connected to 192.168.159.131 (192.168.159.131) port 3000 (#0)
> GET /service-boilerplate/healthz HTTP/1.1
> Host: 192.168.159.131:3000
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-DNS-Prefetch-Control: off
< X-Frame-Options: SAMEORIGIN
< Strict-Transport-Security: max-age=15552000; includeSubDomains
< X-Download-Options: noopen
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Access-Control-Allow-Origin: *
< Date: Sat, 02 May 2020 20:00:58 GMT
< Connection: keep-alive
< Content-Length: 0
< 
* Connection #0 to host 192.168.159.131 left intact
```
#### `/readiness` for readiness test
This is a special route and can be used for defining readiness probe. This route will always respond back with response code `200`. This route can be tuned more according to the service component requirement.
```
$ curl http://192.168.159.131:3000/service-boilerplate/readiness -v
*   Trying 192.168.159.131...
* TCP_NODELAY set
* Connected to 192.168.159.131 (192.168.159.131) port 3000 (#0)
> GET /service-boilerplate/readiness HTTP/1.1
> Host: 192.168.159.131:3000
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-DNS-Prefetch-Control: off
< X-Frame-Options: SAMEORIGIN
< Strict-Transport-Security: max-age=15552000; includeSubDomains
< X-Download-Options: noopen
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Access-Control-Allow-Origin: *
< Date: Sat, 02 May 2020 20:04:23 GMT
< Connection: keep-alive
< Content-Length: 0
< 
* Connection #0 to host 192.168.159.131 left intact
```
#### `/startup` for startup probe
This is a special route and can be used for defining startup probe. This route will always respond back with response code `200`. This route can be tuned more according to the service component requirement.
```
$ curl http://192.168.159.131:3000/service-boilerplate/startup -v
*   Trying 192.168.159.131...
* TCP_NODELAY set
* Connected to 192.168.159.131 (192.168.159.131) port 3000 (#0)
> GET /service-boilerplate/startup HTTP/1.1
> Host: 192.168.159.131:3000
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-DNS-Prefetch-Control: off
< X-Frame-Options: SAMEORIGIN
< Strict-Transport-Security: max-age=15552000; includeSubDomains
< X-Download-Options: noopen
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Access-Control-Allow-Origin: *
< Date: Sat, 02 May 2020 20:07:27 GMT
< Connection: keep-alive
< Content-Length: 0
< 
* Connection #0 to host 192.168.159.131 left intact
```
## Building and Running Docker Images
This application comes with built-in Docker support and it is built grounds up on docker container. Along with the container engine, application is also tested on Kubernetes cluster. 

### Building Docker image
Execute following command to built docker image:
```
$ docker build -t service-boilerplate-api .
Sending build context to Docker daemon  119.3kB
Step 1/17 : ARG NODE_VERSION=12.16.2-slim
Step 2/17 : FROM node:${NODE_VERSION}
 ---> 143b043e4cff
Step 3/17 : LABEL "authur"="openfintechlab.com"       "source-repo"="https://gitlab.com/openfintechlab/medulla/applications/service-boilerplate-api"       "copyright"="Copyright 2020-2022 Openfintechlab, Inc. All rights reserved."
 ---> Using cache
 ---> 5b81c5b0620a
Step 4/17 : ARG NODE_ENV=production
 ---> Using cache
 ---> 42b93b82e895
Step 5/17 : ENV NODE_ENV $NODE_ENV
 ---> Using cache
 ---> c33cfa4cc60e
Step 6/17 : ARG PORT=3000
 ---> Using cache
 ---> 832ea92d9dbb
Step 7/17 : ENV PORT $PORT
 ---> Using cache
 ---> 35903fc26c2d
Step 8/17 : RUN npm i npm@latest -g
 ---> Using cache
 ---> fc28161d0339
Step 9/17 : RUN mkdir /opt/node_app && chown node:node /opt/node_app
 ---> Using cache
 ---> ea3a0794e5ba
Step 10/17 : WORKDIR /opt/node_app
 ---> Using cache
 ---> 7d4d105c6df0
Step 11/17 : USER node
 ---> Using cache
 ---> 9f75543a299c
Step 12/17 : COPY package.json package-lock.json* ./
 ---> 8ebb23ce0ac4
Step 13/17 : RUN npm install --no-optional && npm cache clean --force
 ---> Running in 0c8c1c6c2bb7
added 95 packages from 60 contributors and audited 334 packages in 1.898s
found 0 vulnerabilities

npm WARN using --force I sure hope you know what you are doing.
Removing intermediate container 0c8c1c6c2bb7
 ---> a25c82682e83
Step 14/17 : ENV PATH /opt/node_app/node_modules/.bin:$PATH
 ---> Running in 4235aa1654f5
Removing intermediate container 4235aa1654f5
 ---> 8001ba120dd7
Step 15/17 : WORKDIR /opt/node_app/app
 ---> Running in fa601bdcde6f
Removing intermediate container fa601bdcde6f
 ---> e22d96b8882c
Step 16/17 : COPY . .
 ---> 7c1224bf3aa1
Step 17/17 : CMD [ "node", "./index.js" ]
 ---> Running in 24a121e17631
Removing intermediate container 24a121e17631
 ---> 01df02d71b03
Successfully built 01df02d71b03
Successfully tagged service-boilerplate-api:latest
```

### Building the image with different base image
You can provide build arguments to the `docker build` commands to pass following variables:
1. `NODE_VERSION` : Base image (exp; 12.16.2-slim) 
2. `NODE_ENV` : Node run mode (e.g. 'production')
3. `PORT` : Port which will be exposed by the container.
> NOTE: This variable is different from the environment variable used in the application configuration

Example, following command will build the container using `13.14.0-slim` as a base image tag:
```
$ docker build --build-arg NODE_VERSION=13.14.0-slim -t  service-boilerplate .
Sending build context to Docker daemon  121.9kB
Step 1/17 : ARG NODE_VERSION=12.16.2-slim
Step 2/17 : FROM node:${NODE_VERSION}
13.14.0-slim: Pulling from library/node
b248fa9f6d2a: Already exists 
dffc92453adc: Already exists 
db284201f3ba: Pull complete 
b39ba9d9f5e0: Pull complete 
3deb7f9802b7: Pull complete 
Digest: sha256:11bd13bd8b4d2b812059ffa9eb1db941057c7994938f6c7c66fe1d73278c876f
Status: Downloaded newer image for node:13.14.0-slim
 ---> a236ce6370c5
Step 3/17 : LABEL "authur"="openfintechlab.com"       "source-repo"="https://gitlab.com/openfintechlab/medulla/applications/service-boilerplate-api"       "copyright"="Copyright 2020-2022 Openfintechlab, Inc. All rights reserved."
 ---> Running in d4f8ccce0569
 .......................................
 .......................................
 .......................................
 .......................................
```

### Running on kubernetees

```
$ kubectl apply -f k8s-pod.yaml 
pod/service-boilerplate created
service/service-boilerplate created
$ kubectl get po
NAME                  READY   STATUS    RESTARTS   AGE
service-boilerplate   1/1     Running   0          34s
$ kubectl get service -o wide
NAME                  TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE   SELECTOR
kubernetes            ClusterIP   10.152.183.1     <none>        443/TCP          47h   <none>
service-boilerplate   NodePort    10.152.183.157   <none>        3000:32392/TCP   76s   run=service-boilerplate
```
You can test the api using following command:
```
$ curl http://192.168.159.131:32392/service-boilerplate/healthz -v
*   Trying 192.168.159.131...
* TCP_NODELAY set
* Connected to 192.168.159.131 (192.168.159.131) port 32392 (#0)
> GET /service-boilerplate/healthz HTTP/1.1
> Host: 192.168.159.131:32392
> User-Agent: curl/7.58.0
> Accept: */*
> 
< HTTP/1.1 200 OK
< X-DNS-Prefetch-Control: off
< X-Frame-Options: SAMEORIGIN
< Strict-Transport-Security: max-age=15552000; includeSubDomains
< X-Download-Options: noopen
< X-Content-Type-Options: nosniff
< X-XSS-Protection: 1; mode=block
< Access-Control-Allow-Origin: *
< Date: Sun, 03 May 2020 10:58:42 GMT
< Connection: keep-alive
< Content-Length: 0
< 
* Connection #0 to host 192.168.159.131 left intact
```


# Component Specifications

## Directory Structure
```
service-master-ora/
├── bin
│   ├── config
│   │   ├── AppConfig.d.ts
│   │   ├── AppConfig.js
│   │   └── AppConfig.js.map
│   ├── index.d.ts
│   ├── index.js
│   ├── index.js.map
│   ├── mapping
│   │   └── bussObj
│   │       ├── Response.d.ts
│   │       ├── Response.js
│   │       └── Response.js.map
│   ├── routes
│   │   ├── routes.d.ts
│   │   ├── routes.js
│   │   └── routes.js.map
│   └── utils
│       ├── ExpressApp.d.ts
│       ├── ExpressApp.js
│       ├── ExpressApp.js.map
│       ├── Logger.d.ts
│       ├── Logger.js
│       └── Logger.js.map
├── CHANGELOG.md
├── Dockerfile
├── docs
│   └── readme.md
├── k8s-pod-dev.yaml
├── k8s-pod.yaml
├── LICENSE.md
├── package.json
├── README.md
├── skaffold.yaml
├── src
│   ├── config
│   │   └── AppConfig.ts
│   ├── index.ts
│   ├── mapping
│   │   ├── bussObj
│   │   │   ├── Enttity.ts.txt
│   │   │   └── Response.ts
│   │   ├── EnttityController.ts.txt
│   │   └── simulator
│   │       └── simulator.ts.txt
│   ├── routes
│   │   └── routes.ts
│   ├── tests
│   │   └── JestTestCase.ts.text
│   └── utils
│       ├── ExpressApp.ts
│       └── Logger.ts
├── tsconfig.json
└── yarn.lock
```

## Built With
* [Skaffold](https://skaffold.dev/) - Local kubernetes development
* [Node](https://nodejs.org/en/) - Unddeline code framework
* [Docker](https://www.docker.com/) - Container build engine

## Libraries used

| Framework                   | Reason                                           |
|-----------------------------|:-------------------------------------------------|   
| express                     | Underline web application framework              |
| dotenv                      | Framework for loading environment variables      |
| cors                        | Cross Origin Resource Sharing configuration      |
| helmet                      | Framework for adding security headers            |
| http-status                 | Utility to interact with the status codes        |
| log4js                      | Underline logging framework                      |
| morgan                      | HTTP Request logger middleware for nodejs        |
| xss-clean                   | Middleware to sanitized user input               |
| nodemon (dev)               | Automatic file monitoring and reloading util     |
| typescript                  | Application is developed in typescript           |
| yarn                        | yarn modules for managing modules                |

## Authors
* **Muhammad Furqan** - *Initial work* - [Openfintechlab.com](https://openfintechlab.com)

