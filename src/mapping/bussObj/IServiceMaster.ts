export default interface IServiceMaster{
    "service-master": {        
        srv_id: string;              
        name: string;            
        title: string;               
        group_slug: string;          
        group_name: string;          
        imp_tech_uri: string;        
        source_uri: string;          
        ctx_root_uri: string;        
        sdk_doc_uri: string;         
        api_doc_uri: string;         
        visibility: string;          
        type: string;              
        lifecycle: string;           
        discussion_chl_uri: string;  
        tags: string;                
        doc_md_b64: string;          
        doc_md_file_name: string;                    
    }
}