/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                  from "express";
import util                     from "util";
import {AppHealth}              from "../config/AppConfig";
import logger                   from "../utils/Logger";
import ServiceMasterController  from "../mapping/ServiceMasterController";
import ServiceMasterReqSchemaModel from "../mapping/validators/Service_Master_GBO.validator";
import {PostRespBusinessObjects}   from "../mapping/bussObj/Response";



const router: any = express.Router();


/**
 * Route to create a service
 */
router.post('/', async (request:any,res:any) => {        
    res.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for adding service-master`);
    const srvMasterCtrol:ServiceMasterController = new ServiceMasterController();
    try{        
        let parsedObj = new ServiceMasterReqSchemaModel().validateCreate(request.body);
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj.metadata,{compact:true,colors:true, depth: null})}`)
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            res.status(500);
            res.send(parsedObj);
        }else{
            res.status(201);
            const resBO:string = await srvMasterCtrol.create(JSON.stringify(parsedObj),request.query.generateid !== undefined);    
            res.send(resBO);
        }                        
    }catch(error){
        logger.debug(`Error occured while creating specific service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        if(error.metadata === undefined){
            res.status(500);
        }else if(error.metadata.status === '8153'){
            res.status(404);            
        }else{
            res.status(500);
        }
        if(error.metadata !== undefined){            
            res.send(error);
        }else{            
            res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("9901","Error while processing the request"));
        } 
    }        
});


/**
 * Route to delete a service using srv_id / service id
 */
router.delete('/:id', async(request:any,res:any) =>{
    res.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for deleting service-master`);
    const srvMasterCtrol:ServiceMasterController = new ServiceMasterController();
    try{
        const resBO: string = await srvMasterCtrol.delete(request.params.id);    
        
        res.status(201);
        res.send(resBO);
    }catch(error){        
        logger.debug(`Error occured while deleting specific service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        if(error.metadata === undefined){
            res.status(500);
        }else if(error.metadata.status === '8153'){
            res.status(404);            
        }else{
            res.status(500);
        }
        if(error.metadata !== undefined){            
            res.send(error);
        }else{            
            res.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("9901","Error while processing the request"));
        } 
    }    
});

/**
 * Fetch specific Service-Master instance
 */
router.get('/:id',async(request:any, response:any)=> {    
    logger.info(`Procedure called for fetching all service-master details`);
    response.set("Content-Type","application/json; charset=utf-8");    
    let id:string = request.params.id;
    const srvMasterCtrol:ServiceMasterController = new ServiceMasterController();
    try{
        let output = await srvMasterCtrol.get(id);
        logger.debug(`Response received: ${util.inspect(output,{compact:true,colors:true, depth: null})}`);
        response.status(200);
        response.send(output);
    }catch(error){
        logger.debug(`Error occured while fetching specific service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        if(error.metadata === undefined){
            response.status(500);
        }else if(error.metadata.status === '8153'){
            response.status(404);            
        }else{
            response.status(500);
        }
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("9901","Error while processing the request"));
        } 
    }
    
});

/**
 * Get all service-master objects according to from and to page number
 * requested parameters:
 * param#1: ?total=10  upto maximum 100; through error beyond 100
 * param#: ?from=0     from count; if from=0, total will start from 0 to the value specified
 * param: ?visbility='private' | 'public'
 * param: ?lifecycle=''
 */
router.get('/', async (request: any, response:any) => {
    // Get from and to
    logger.info(`Procedure called for fetching all service-master details`);
    response.set("Content-Type","application/json; charset=utf-8");    
    let from:number, to:number;
    let visibility:string, lifecycle:string;
    from = (request.query.from)? request.query.from : undefined;
    to = (request.query.to)? request.query.to : undefined;
    visibility = (request.query.visibility)? request.query.visibility : undefined;
    lifecycle = (request.query.lifecycle)? request.query.lifecycle : undefined;
    try{
        const srvMasterCtrol:ServiceMasterController = new ServiceMasterController();
        let srvResp = await srvMasterCtrol.getAll(from,to, visibility, lifecycle);
        response.status(200);
        response.send(srvResp);
    }catch(error){
        logger.debug(`Error occured while fetching all service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        if(error.metadata === undefined){
            response.status(500);
        }else if(error.metadata.status === '8153'){
            response.status(404);            
        }else{
            response.status(500);
        }
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("9901","Error while processing the request"));
        } 
    }

});


/**
 * Route to update the service-master
 */
router.put('/', async(request:any,response:any) => {
    logger.info(`Update service-master called`);
    logger.debug(`Call to update service-master initiated. request recevied ${util.inspect(request.data,{compact:true,colors:true, depth: null})}`);    
    response.set("Content-Type","application/json; charset=utf-8");    
    try{
        let parsedObj = new ServiceMasterReqSchemaModel().validateUpdate(request.body);
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj.metadata,{compact:true,colors:true, depth: null})}`)
        const srvMasterCtrol:ServiceMasterController = new ServiceMasterController();
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(500);
            response.send(parsedObj)
        }else{
            response.status(201);
            const resBO:string = await srvMasterCtrol.update(JSON.stringify(request.body));    
            response.send(resBO);
        } 

    }catch(error){
        logger.debug(`Error occured while updating specific service-master  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        if(error.metadata === undefined){
            response.status(500);
        }else if(error.metadata.status === '8153'){
            response.status(404);            
        }else{
            response.status(500);
        }
        if(error.metadata !== undefined){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("9901","Error while processing the request"));
        } 
    }    
});



export default router;