
/**
  * @copyright Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of the framework
 * @description
 * Module for loading configuration from .env file
 * @author Muhammad Furqan <furqan.baqai@openfintechlab.com>
 * 
 */

import * as _       from "lodash";
import dotenv       from "dotenv";
import StaticVars   from "./StaticVars";


const package_json = require('../../package.json');
const node_package_name = package_json.name;

/**
 * Class for encapstulating Application Configuration
 */
export default abstract class AppConfig {
    private static package_json = require(`../../package.json`);
    private static node_package_name = package_json.name;
    private static node_package_version = package_json.version;
    private static cfgObject: any;

    /**
     * @summary Loads configuration from environment variable
     */
    public static validateConfig():boolean{               
        if(process.env.LOAD_FROM_CONFIG){
            let envPath = { path: process.cwd() +`/${process.env.CONFIG_FILE}` };            
            dotenv.config(envPath); // loads config file from environment variable
        }              
        this.cfgObject = process.env;
        let keyCount:number = Object.keys(this.cfgObject).length ;
        AppHealth.config_loaded = true;   
        return keyCount > 0;

    }

     /**
     * Get Context Root of service component. 
     * <b>NOTE:</b> By default, context root will always be the package name set in package.json
     */
    public static get contextRoot(): string{                
        if(this.config[StaticVars.ConfigVars.contextRoot] === undefined ||
            this.config[StaticVars.ConfigVars.contextRoot].length <= 0){                
            return '/' + node_package_name;
        }else{
            return this.config[StaticVars.ConfigVars.contextRoot] ;
        }     
    }

    /**
    * Get service config JS object
    */
    public static get config(): any{
        return this.cfgObject;
    }
}

export const AppHealth = {
    "config_loaded": false as boolean,
    "express_loaded": false as boolean,
    "config_uri_loaded": false as boolean,
    "reload_required": false as boolean,
    "started": false as boolean,
    "ready": false as boolean,
    "isLive": false as boolean
}