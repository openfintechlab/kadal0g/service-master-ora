# 
# Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
# Description: 
# Generic container file for nodejs container
# Arguments:
#  - NODE_VERSION=Node's base container version number (default: 12.16.2-slim)
#  - NODE_ENV=production|development (default: production)
#  - PORT=Express listener port on which traffic will be exposed

ARG NODE_VERSION=12.16.2-slim
FROM node:${NODE_VERSION}

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

# default to port 3000 for node
ARG PORT=3000
ENV PORT $PORT 

#
# Builder container
#
FROM node:${NODE_VERSION} AS builder

# Creating application directory
RUN mkdir /opt/node_app && \
      mkdir /opt/node_app/bin && \
      mkdir /oralibs && \ 
      chown -R node:node /oralibs && \
      chown -R node:node /opt/node_app && \
      apt-get update && \
      apt-get install libaio1 -y

WORKDIR /opt/node_app

USER node
COPY --chown=node:node  package.json yarn.lock tsconfig.json .snyk ./
COPY --chown=node:node  ./libs/instantclient_19_8_lin_x86_64/* /oralibs/
COPY --chown=node:node  ./src src/
COPY --chown=node:node  ./docs docs/


RUN npm config set scripts-prepend-node-path auto && \
      yarn install --force && \
      chown -R node:node * && \
      yarn build && \
      chown -R node:node *       

ENV PATH /opt/node_app/node_modules/.bin:$PATH
ENV LD_LIBRARY_PATH /oralibs


CMD [ "node", "./bin/index.js" ]
