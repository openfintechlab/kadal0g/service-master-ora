import Joi from "joi";
import  logger                          from "../../utils/Logger";
import {PostRespBusinessObjects}        from "../bussObj/Response";

/**
 * Validator class for validating ServiceMaster Schema model
 */
export default class ServiceMasterReqSchemaModel{
    ServiceMasterReqSchema_CRT = Joi.object({
        "service-master": {
            srv_id: Joi.string().min(8).max(56).required(),
            name: Joi.string().min(8).max(128).required(),
            title: Joi.string().min(16).max(256).required(),
            group_slug: Joi.string().min(8).max(128).required(),
            group_name: Joi.string().min(8).max(256).required(),
            imp_tech_uri: Joi.string().max(256).required(),
            source_uri: Joi.string().max(256),
            hostname: Joi.string().max(128).required(),
            port: Joi.number().port().required(),
            ctx_root_uri: Joi.string().max(256),
            sdk_doc_uri: Joi.string().max(256),
            api_doc_uri: Joi.string().max(256),
            visibility: Joi.any().valid('private','public').required(),
            type: Joi.any().valid('infra','functional','business','technical','atomic').required(),
            lifecycle: Joi.any().valid('sit','dev','uat','production').required(),
            discussion_chl_uri: Joi.string().max(256),
            tags:Joi.string().max(128),
            doc_md_b64: Joi.string().base64(),
            doc_md_file_name: Joi.string().max(64)
        }
    }).with('doc_md_b64','doc_md_file_name');

    ServiceMasterReqSchema_UPD = Joi.object({
        "service-master": {
            srv_id: Joi.string().min(8).max(56).required(),
            name: Joi.string().min(8).max(128),
            title: Joi.string().min(16).max(256),
            group_slug: Joi.string().min(8).max(128),
            group_name: Joi.string().min(8).max(256),
            imp_tech_uri: Joi.string().max(256),
            source_uri: Joi.string().max(256),
            hostname: Joi.string().max(128).required(),
            port: Joi.number().port().required(),
            ctx_root_uri: Joi.string().max(256),
            sdk_doc_uri: Joi.string().max(256),
            api_doc_uri: Joi.string().max(256),
            visibility: Joi.any().valid('private','public'),
            type: Joi.any().valid('infra','functional','business','technical','atomic'),
            lifecycle: Joi.any().valid('sit','dev','uat','production'),
            discussion_chl_uri: Joi.string().max(256),
            tags:Joi.string().max(128),
            doc_md_b64: Joi.string().base64(),
            doc_md_file_name: Joi.string().max(64)
        }
    });

    /**
     * Validate the provided JSON object and provide the parsed schema for CREATE function
     * @example
     *  let sample:string = '{"service-master":{"srv_id": "123456789"}}';
     *  try{
     *      let output = new ServiceMasterReqSchemaModel().validate(JSON.parse(sample));
     *      // here output = parsed JSON object
     *  }catch(error){
     *      console.log(error);
     *      // error:  {"metadata":{"status":"8154","description":"Schema validation error","trace":[{"source":"schema-validation","description":"service-master.name is required"}]}}        
     *  }        
     * @param req (Object) JSON object containing valid ServiceMasterSchemaModel
     */
    public validateCreate(req:any){
        let {error,value} = this.ServiceMasterReqSchema_CRT.validate(req);
        if(error){
            logger.error(`Validation error ${error}`);            
            let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            return JSON.parse(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8154","Schema validation error",trace));                        
        }else{            
            return value;
        }
    }

    
    /**
     * Validate the provided JSON object and provide the parsed schema for UPDATE function
     * @example
     *  let sample:string = '{"service-master":{"srv_id": "123456789"}}';
     *  try{
     *      let output = new ServiceMasterReqSchemaModel().validate(JSON.parse(sample));
     *      // here output = parsed JSON object
     *  }catch(error){
     *      console.log(error);
     *      // error:  {"metadata":{"status":"8154","description":"Schema validation error","trace":[{"source":"schema-validation","description":"service-master.name is required"}]}}        
     *  }        
     * @param req (Object) JSON object containing valid ServiceMasterSchemaModel
     */
    public validateUpdate(req:any){
        let {error,value} = this.ServiceMasterReqSchema_UPD.validate(req);
        if(error){
            logger.error(`Validation error ${error}`);            
            let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            return JSON.parse(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8154","Schema validation error",trace));                        
        }else{            
            return value;
        }
    }
    

}